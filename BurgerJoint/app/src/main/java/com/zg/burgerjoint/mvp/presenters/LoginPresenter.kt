package com.zg.burgerjoint.mvp.presenters

/**
 * Created by ktmmoe on 11, July, 2020
 **/
interface LoginPresenter {
    fun onTapLogin(userName: String, password : String)
}