package com.zg.burgerjoint.activities

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import com.google.android.material.snackbar.Snackbar
import com.zg.burgerjoint.R
import com.zg.burgerjoint.activities.BurgerDetailsActivity.Companion.newIntent
import com.zg.burgerjoint.activities.MainActivity.Companion.newIntent
import com.zg.burgerjoint.activities.CartActivity.Companion.newIntent
import com.zg.burgerjoint.mvp.presenters.LoginPresenter
import com.zg.burgerjoint.mvp.presenters.impls.LoginPresenterImpl
import com.zg.burgerjoint.mvp.views.LoginView
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by ktmmoe on 11, July, 2020
 **/
class LoginActivity() : BaseActivity(), LoginView, Parcelable {


    private lateinit var mPresenter: LoginPresenter

    constructor(parcel: Parcel) : this() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setUpPresenter()
        setUpListeners()
    }

    private fun setUpPresenter() {
        mPresenter = getPresenter<LoginPresenterImpl, LoginView>()
    }

    private fun setUpListeners() {
        btnLogin.setOnClickListener {
            mPresenter.onTapLogin(etUserName.text.toString(), etPassword.text.toString())
        }
    }

    override fun navigateToMainScreen() {
        startActivity(MainActivity.newIntent(this))
    }

    override fun showErrorMessage(message: String) {
        Snackbar.make(window.decorView, message, Snackbar.LENGTH_LONG).show()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LoginActivity> {
        override fun createFromParcel(parcel: Parcel): LoginActivity {
            return LoginActivity(parcel)
        }

        override fun newArray(size: Int): Array<LoginActivity?> {
            return arrayOfNulls(size)
        }
    }
}